
import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Carousel from 'react-bootstrap/Carousel';

function App() {
  return (
    <div class="container">
      <div class="item1" id="header"><p>Carousel</p></div>
      <div class="item2">
        <div id="categories">
          <h3>Categories</h3>
          <select name="photos" id="photos" onChange="window.location.href=this.value">
            <option value="#nature">Nature</option>
            <option value="Home">Home</option>
            <option value="School">School</option>
            <option value="Food">LifeStyle</option>
          </select>
        </div>
        <div id="files">
          <h3>Files</h3>
        </div>
      </div>
      <div class="item3">
      <Carousel id="nature">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1585720743620-ef890066eb9f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE3fHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 1"
        />
        <Carousel.Caption>
          <h3>image 1</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1631623394471-7386b50b149c?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDUzfDZzTVZqVExTa2VRfHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 2"
        />
        <Carousel.Caption>
          <h3>image 2</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1631763419267-50b85246b789?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDExfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 3"
        />
        <Carousel.Caption>
          <h3>image 3</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1632041398953-1ec058af29bb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDE4fDZzTVZqVExTa2VRfHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 4"
        />
        <Carousel.Caption>
          <h3>image 4</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1632038195014-a8a2c8a00914?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDIwfDZzTVZqVExTa2VRfHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 5"
        />
        <Carousel.Caption>
          <h3>image 5</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>
      
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1631623394471-7386b50b149c?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDUzfDZzTVZqVExTa2VRfHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 6"
        />
        <Carousel.Caption>
          <h3>image 6</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1608098742863-48d976e45e3c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 7"
        />
        <Carousel.Caption>
          <h3>image 7</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://images.unsplash.com/photo-1572970291209-2be05cc8368d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE0fHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=500&q=60"
          alt="taken from unsplash.com 8"
        />
        <Carousel.Caption>
          <h3>image 8</h3>
          <p>This image was taken from unsplash.com</p>
        </Carousel.Caption>
      </Carousel.Item>
      </Carousel>
      </div>
      <div class="item4">Footer</div>
    </div>
  );
}

export default App;
